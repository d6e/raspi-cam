#!/bin/bash
sudo apt-get install cmake liblivemedia-dev liblog4cpp5-dev libv4l-dev
wget www.live555.com/liveMedia/public/live555-latest.tar.gz
tar xfz live555-latest.tar.gz
cd live/
./genMakefiles linux
make CPPFLAGS=-DALLOW_RTSP_SERVER_PORT_REUSE=1
sudo make install
cd
git clone https://github.com/mpromonet/v4l2rtspserver.git
cd v4l2rtspserver/
cmake .
make
sudo make install
sudo ln -s $PWD/v4l2rtspserver.service /etc/systemd/system/v4l2rtspserver.service
